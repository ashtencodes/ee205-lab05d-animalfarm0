#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "addCats.h"

int addCat( char name[], enum Gender gender, enum Breed breed, bool isFixed, float weight ){
    if(currentCatNum < MAX_NUM_CATS){
        assert(0 < strlen(name));
        assert(strlen(name) < NAME_LEN_MAX);
        assert(weight > 0);

        bool isDuplicate = false;
        for( int indexNum = 0; indexNum < MAX_NUM_CATS; indexNum++ ){
            if( strcmp(nameArray[indexNum], name) == 0 ){
                isDuplicate = true;
            }
        }

        if(!isDuplicate){
            strcpy(nameArray[currentCatNum], name);
            genderArray[currentCatNum] = gender;
            breedArray[currentCatNum] = breed;
            isFixedArray[currentCatNum] = isFixed;
            weightArray[currentCatNum] = weight;

            currentCatNum += 1;
        } else {
            printf("Duplicate name! [%s]\n", name);
        }

    }
    return currentCatNum;
 };