#include "reportCats.h"
#include <stdio.h>
#include <string.h>

void printCat(int indexNum){
    if( indexNum < 0 || indexNum > MAX_NUM_CATS ){
       printf("animalFarm0: Bad Cat [%d]\n", indexNum);
    }
    printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", indexNum, nameArray[indexNum], genderArray[indexNum], breedArray[indexNum], isFixedArray[indexNum], weightArray[indexNum]);
}

void printAllCats(){
    for( int indexNum = 0; indexNum < MAX_NUM_CATS; indexNum++ ){
        printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", indexNum, nameArray[indexNum], genderArray[indexNum], breedArray[indexNum], isFixedArray[indexNum], weightArray[indexNum]);
    }
}

int findCat(char name[]){
    int targetIndex;
    for( int indexNum = 0; indexNum < MAX_NUM_CATS; indexNum++ ){
        if( strcmp(nameArray[indexNum], name) == 0 ){
            targetIndex = indexNum;
        }
    }
    printf("animalFarm0: cat index: [%d]\n", targetIndex);
    return targetIndex;
}