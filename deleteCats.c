#include "deleteCats.h"
#include <string.h>

void deleteAllCats(){
    for( int indexNum = 0; indexNum < MAX_NUM_CATS; indexNum++ ){
        memset(nameArray[indexNum], 0, MAX_NUM_CATS);
    }
    memset(genderArray, 0, MAX_NUM_CATS);
    memset(breedArray, 0, MAX_NUM_CATS);
    memset(isFixedArray, false, MAX_NUM_CATS);
    memset(weightArray, 0, MAX_NUM_CATS);
}

void deleteCat(int index){
    for(int x = index; x < MAX_NUM_CATS - 1; x++){
        strcpy(nameArray[x], nameArray[x + 1]);
        genderArray[x] = genderArray[x + 1];
        breedArray[x] = breedArray[x + 1];
        isFixedArray[x] = isFixedArray[x + 1];
        weightArray[x] = weightArray[x + 1];
    }
}