#include "updateCats.h"
#include <stdio.h>
#include <string.h>

void updateCatName(int index, char name[]){
    bool isDuplicate = false;
    for( int indexNum = 0; indexNum < MAX_NUM_CATS; indexNum++ ){
        if( strcmp(nameArray[indexNum], name) == 0 ){
            isDuplicate = true;
        }
    }

    if(!isDuplicate){
        strcpy(nameArray[index], name);
    } else {
        printf("Duplicate name! [%s]\n", name);
    }
}

void fixCat(int index){
    if(isFixedArray[index] == false){
        printf("Neutered [%s]!\n", nameArray[index]);
        isFixedArray[index] = true;
    }
}

void updateCatWeight (int index, float newWeight){
    if (newWeight > 0){
        weightArray[index] = newWeight;
    } else {
        printf("Weight must be greater than 0!");
    }
}
