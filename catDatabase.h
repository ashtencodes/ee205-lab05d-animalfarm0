#pragma once
#include <stdbool.h>

#define MAX_NUM_CATS 30
#define NAME_LEN_MAX 30

extern int currentCatNum;

enum Gender { UNKNOWN_GENDER, MALE, FEMALE };
enum Breed { UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHINX };

extern char nameArray[][NAME_LEN_MAX];
extern enum Gender genderArray[];
extern enum Breed breedArray[];
extern bool isFixedArray[];
extern float weightArray[];

void clearDataBase();