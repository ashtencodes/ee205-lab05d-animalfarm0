#pragma once
#include "catDatabase.h"

void updateCatName(int index, char name[]);

void fixCat(int index);

void updateCatWeight (int index, float newWeight);
